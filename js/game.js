"use strict"
/*
Обявление переменых и констант, создание объектов в канвасе
*/
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var WIDTH = canvas.width;
var HEIGHT = canvas.height;

var BASKET = new Image();
var BACKGROUND = new Image();
var GROUND = new Image();
var MANDARIN = new Image();
BASKET.src = "img/basket.png";
MANDARIN.src = "img/mandarin.png";
GROUND.src = "img/ground.png";
BACKGROUND.src = "img/fon.png";

var players = [
    { name: 'Navi',
      score: 15,
    },
    { name: 'Black_Mud',
      score: 52,
    },
    { name: 'Drago77',
      score: 133,
    }
];
/*
Проверка localStorage на наличие масива игроков
*/
var returnDat = JSON.parse(localStorage.getItem("players"));
    if (returnDat !== null) {
        players =  returnDat;
};  

var topPlayers = []; 
var motionSpeed = 50;
var follSpeed = 2.5;    
var userScore = 0;
var life = 10;
var coordinates = [];
coordinates[0] = {
    x: 350,
    y: 0,
}
var xPos = 300;
var yPos = 0;
var basketX = 300;
var basketY = 380;
var toggle = true;

/*
Поиск объектов в html
*/
var startPanel = document.querySelector(".start-panel");
var buttonStart = document.querySelector(".start-panel_button");
var nameInput = document.querySelector(".start-panel_name");
var name = nameInput.value;
var userName = document.querySelector(".user_name");
var newName = document.querySelector(".new-name");
var topList = document.querySelector(".top-list");

/*
Функция которая в паре с concat() и .sort() будет принимать масив игроков и выбирать 
у которых самое большое количество очков
*/

function compare(a,b) {
    if (a.score > b.score)
      return -1;
    if (a.score < b.score)
      return 1;
    return 0;
};

/*
Функция для показа и измениния топа лучших игроков
*/
function changeTopList (){
    for (var w = 0; w <=2; w++){
        topList.children[w].innerText = topPlayers[w].name + " - " + topPlayers[w].score;
    }
}

/*
Создание топа игроков
*/
var tempList = players.concat().sort(compare);
for (var s = 0; s <=2; s++) {
    if (tempList[s]){            
        topPlayers[s] = tempList[s];
    };
}
changeTopList ();


/*  
Функция для изменения полоски жизней
*/
function showLife () {
    var line = document.querySelector('.line');
    var procent = document.querySelector('.procent');
    procent.textContent = life * 10 + "%";
    line.style.width = life * 10 + "%";
}

/*
Отлов событий и измениние параметров для управления корзиной и для паузы
*/
window.onkeydown = function move(){    
    if(event.keyCode == 37 && toggle !== false){    
        basketX -= motionSpeed;
        if (basketX <= 0) {
            basketX = 0;
        }             
    }  else if(event.keyCode == 39 && toggle !== false){    
        basketX += motionSpeed; 
        if (basketX + BASKET.width >= WIDTH) {
            basketX = WIDTH - BASKET.width;
    }                
    } else if(event.keyCode == 32){    
        toggle = !toggle;                      
    } 
}; 

/*
Функция принимает диапазон чисел и дает случайное целое число в даном диапазоне
*/
function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

/*
Функция постоянного изменения очков
*/
function changeScore (){
    players[players.length - 1].score = userScore;
    tempList = players.concat().sort(compare);
    for (var s = 0; s <=2; s++) {
        if (tempList[s]){            
            topPlayers[s] = tempList[s];
        };
    }
    changeTopList ();
};

/*
Главная функция
- отрисовывает обекты в канвасе (*1)
- создает мандаринки и двигает их (*2)
- отрисовывает жизни (*3)
- сохраняет данные в localStorage(*4)
*/
function draw() {
    /*1*/
    ctx.drawImage(BACKGROUND, 0, 0);
    ctx.drawImage(GROUND, 0, 400); 
    ctx.drawImage(BASKET, basketX, basketY);

    ctx.font = "25px Courier bold";
    ctx.fillStyle = "red";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText("Счет: " + userScore, 50, 30)
    /*2*/
    for (var i = 0; i < coordinates.length; i++) {
        if (coordinates[i].y == 200 ) {
            coordinates.push({ 
                x: randomInteger(0, WIDTH-100),     
                y: 0   
            });            
        } 
        /*если toggle == false это значит что нажата пауза(пробел) либо игрок меняет имя, 
        в этом случае мандаринки перестают падать*/        
        if (toggle) {              
            coordinates[i].y += follSpeed;
        }       
        ctx.drawImage(MANDARIN, coordinates[i].x, coordinates[i].y);
        /*столкновение корзины и мандаринки*/
        if (coordinates[i].x >= basketX 
            && coordinates[i].x + MANDARIN.width <= basketX + BASKET.width 
            && coordinates[i].y >= basketY) {
             userScore += 1;
             changeScore ();                         
             coordinates[i].y = undefined;
        };           
        /*столкновение мандаринки с землей */
        if (coordinates[i].y == HEIGHT - 100) {
            --life; 
            coordinates[i].y = undefined;             
        }  
    }
    showLife(); 
    /*если жизни дойдут до 0 - игра остановится*/
    if (life === 0) {
        var localDat = JSON.stringify(players); 
        localStorage.setItem("players", localDat); 
        return alert ("Finish");
    }    
    /*Функция requestAnimationFrame  для повторного вызова главной функции draw*/
    requestAnimationFrame(draw);   
}



/*
Функция добавления игрока
*/
function addPlayer () {
    var nameInput = document.querySelector(".start-panel_name");
    var nameValue = nameInput.value;
    var userName = document.querySelector(".user_name");    
    userName.value = nameValue;
    players.push({name: nameValue, score: userScore});
}; 
/*
Функция изменения игрока и отлов события нажатия на кнопку "Изменить имя"
*/
function changePlayer (){
    players[players.length - 1].name = userName.value;
};

newName.onclick = function(event){
    event = event || window.event;    
    if (userName.attributes.disabled != undefined && toggle !== false ){
        toggle = !toggle;
        userName.disabled = 0;
        newName.value = "Сохранить";
    } else if (userName.attributes.disabled == undefined && toggle !== true) {
        toggle = !toggle;
        changePlayer ();
        userName.disabled = 1;
        newName.value = "Изменить имя";
    };    
}
/*
Функция для старта при нажатии на кнопку "Start"
*/
buttonStart.onclick = function(event) {
    event = event || window.event;     
    addPlayer (); 
    startPanel.classList.toggle('hidden');    
    draw();
};